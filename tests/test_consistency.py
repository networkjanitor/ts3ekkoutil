import unittest
import inspect

from ts3ekkoutil.parser import create_ekko_parser
from ts3ekkoutil.envconsts import EkkoPropertyNames


class ConsistencyTest(unittest.TestCase):
    def setUp(self):
        self.env_consts_attributes = self._get_nonpy_attribute_values(EkkoPropertyNames)
        self.args = self._get_nonpy_attribute_names(create_ekko_parser().parse_args())

    def _get_nonpy_attribute_names(self, thing):
        attributes = inspect.getmembers(thing, lambda a: not (inspect.iscoroutine(a)))
        return [a[0] for a in attributes if not (a[0].startswith('_'))]

    def _get_nonpy_attribute_values(self, thing):
        attributes = inspect.getmembers(thing, lambda a: not (inspect.iscoroutine(a)))
        return [a[1] for a in attributes if not (a[0].startswith('_'))]

    def test_parameter_conistency(self):
        self.assertEqual(sorted(self.env_consts_attributes), sorted(self.args))


if __name__ == '__main__':
    unittest.main()
